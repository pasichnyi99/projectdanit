

const calcFibonacci = num => {
    if (num <= 1){
        return num
    } else {
        return calcFibonacci(num - 1) + calcFibonacci(num - 2)
    }
}

console.log(calcFibonacci(6))



// function calcFibonacci(num) {
//     return num <= 1 ? num : calcFibonacci(num - 1) + calcFibonacci(num - 2);
// }
//
// console.log( calcFibonacci(6) );